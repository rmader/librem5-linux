From: =?utf-8?q?Guido_G=C3=BCnther?= <agx@sigxcpu.org>
Date: Sun, 14 Jul 2019 13:42:24 +0200
Subject: power: supply: bq25890: Provide battery capacity infomation
MIME-Version: 1.0
Content-Type: text/plain; charset="utf-8"
Content-Transfer-Encoding: 8bit

Use the current battery voltage and an ocv table to provide
battery capacity information.

Signed-off-by: Guido Günther <agx@sigxcpu.org>
---
 drivers/power/supply/bq25890_charger.c | 55 +++++++++++++++++++++++++++++++++-
 1 file changed, 54 insertions(+), 1 deletion(-)

diff --git a/drivers/power/supply/bq25890_charger.c b/drivers/power/supply/bq25890_charger.c
index 07d5935..aefdccf 100644
--- a/drivers/power/supply/bq25890_charger.c
+++ b/drivers/power/supply/bq25890_charger.c
@@ -133,6 +133,10 @@ struct bq25890_device {
 	struct bq25890_init_data init_data;
 	struct bq25890_state state;
 
+	/* ocv capacity table */
+	struct power_supply_battery_ocv_table *cap_table;
+	int table_len;
+
 	struct mutex lock; /* protect state data */
 };
 
@@ -497,7 +501,7 @@ static int bq25890_power_supply_get_property(struct power_supply *psy,
 {
 	struct bq25890_device *bq = power_supply_get_drvdata(psy);
 	struct bq25890_state state;
-	int ret;
+	int ret, ocv;
 
 	bq25890_update_state(bq, psp, &state);
 
@@ -681,6 +685,21 @@ static int bq25890_power_supply_get_property(struct power_supply *psy,
 		val->intval = bq25890_find_val(ret, TBL_TSPCT);
 		break;
 
+	case POWER_SUPPLY_PROP_CAPACITY:
+		if (!bq->cap_table)
+			return -ENODEV;
+
+		ret = bq25890_field_read(bq, F_SYSV); /* read measured value */
+		if (ret < 0)
+			return ret;
+
+		/* converted_val = 2.304V + ADC_val * 20mV (table 10.3.15) */
+		ocv = 2304000 + ret * 20000;
+		val->intval = power_supply_ocv2cap_simple(bq->cap_table,
+							  bq->table_len, ocv);
+		dev_info(bq->dev, "Capacity for %d is %d%%", ocv, val->intval);
+		break;
+
 	default:
 		return -EINVAL;
 	}
@@ -1011,6 +1030,7 @@ static const enum power_supply_property bq25890_power_supply_props[] = {
 	POWER_SUPPLY_PROP_VOLTAGE_NOW,
 	POWER_SUPPLY_PROP_CURRENT_NOW,
 	POWER_SUPPLY_PROP_TEMP,
+	POWER_SUPPLY_PROP_CAPACITY,
 };
 
 static char *bq25890_charger_supplied_to[] = {
@@ -1457,6 +1477,35 @@ static void bq25890_non_devm_cleanup(void *data)
 	}
 }
 
+static int bq25890_battery_init(struct bq25890_device *bq)
+{
+	struct power_supply_battery_ocv_table *table;
+	struct power_supply_battery_info *info;
+	int ret;
+
+	/* battery information is optional */
+	ret = power_supply_get_battery_info(bq->charger, &info);
+	if (ret < 0) {
+		dev_info(bq->dev, "No battery found: %d", ret);
+		return 0;
+	}
+
+	/* We use a single OCV table at 20 C */
+	table = power_supply_find_ocv2cap_table(info, 20, &bq->table_len);
+	if (!table)
+		return -EINVAL;
+
+	bq->cap_table = devm_kmemdup(bq->dev, table,
+				     bq->table_len * sizeof(*table),
+				     GFP_KERNEL);
+	if (!bq->cap_table) {
+		power_supply_put_battery_info(bq->charger, info);
+		return -ENOMEM;
+	}
+	power_supply_put_battery_info(bq->charger, info);
+	return 0;
+}
+
 static int bq25890_probe(struct i2c_client *client)
 {
 	struct device *dev = &client->dev;
@@ -1536,6 +1585,10 @@ static int bq25890_probe(struct i2c_client *client)
 	if (ret)
 		return ret;
 
+	ret = bq25890_battery_init(bq);
+	if (ret < 0)
+		return dev_err_probe(dev, ret, "Failed get battery information\n");
+
 	if (!IS_ERR_OR_NULL(bq->usb_phy)) {
 		INIT_WORK(&bq->usb_work, bq25890_usb_work);
 		bq->usb_nb.notifier_call = bq25890_usb_notifier;
