From: Sebastian Krzyszkowiak <sebastian.krzyszkowiak@puri.sm>
Date: Sat, 22 Jan 2022 17:42:00 +0100
Subject: power: supply: max17042_battery: Use dSOCi on MAX17055

---
 drivers/power/supply/max17042_battery.c | 37 ++++++++++++++++++++++++++++++---
 1 file changed, 34 insertions(+), 3 deletions(-)

diff --git a/drivers/power/supply/max17042_battery.c b/drivers/power/supply/max17042_battery.c
index 17ac2ab..60de23c 100644
--- a/drivers/power/supply/max17042_battery.c
+++ b/drivers/power/supply/max17042_battery.c
@@ -26,6 +26,7 @@
 /* Status register bits */
 #define STATUS_POR_BIT         (1 << 1)
 #define STATUS_BST_BIT         (1 << 3)
+#define STATUS_DSOCI_BIT       (1 << 7)
 #define STATUS_VMN_BIT         (1 << 8)
 #define STATUS_TMN_BIT         (1 << 9)
 #define STATUS_SMN_BIT         (1 << 10)
@@ -37,6 +38,7 @@
 
 /* Interrupt mask bits */
 #define CONFIG_ALRT_BIT_ENBL	(1 << 2)
+#define CONFIG2_DSOCI_BIT_ENBL	(1 << 7)
 
 #define VFSOC0_LOCK		0x0000
 #define VFSOC0_UNLOCK		0x0080
@@ -51,6 +53,8 @@
 
 #define MAX17042_VMAX_TOLERANCE		50 /* 50 mV */
 
+#define MAX17042_CRITICAL_SOC		0x03
+
 struct max17042_chip {
 	struct i2c_client *client;
 	struct regmap *regmap;
@@ -861,6 +865,28 @@ static void max17042_set_soc_threshold(struct max17042_chip *chip, u16 off)
 	regmap_write(map, MAX17042_SALRT_Th, soc_tr);
 }
 
+static void max17042_set_critical_soc_threshold(struct max17042_chip *chip)
+{
+	struct regmap *map = chip->regmap;
+	u32 soc;
+
+	regmap_read(map, MAX17042_RepSOC, &soc);
+	regmap_write(map, MAX17042_SALRT_Th,
+			((soc >> 8) >= MAX17042_CRITICAL_SOC) ?
+			0xff00 + MAX17042_CRITICAL_SOC : 0xff00);
+}
+
+static void max17042_update_soc_threshold(struct max17042_chip *chip)
+{
+	if (chip->chip_type == MAXIM_DEVICE_TYPE_MAX17055) {
+		/* on max17055 we can use dSOCi instead,
+		 * so SALRT can be used to wake up on critical SOC */
+		max17042_set_critical_soc_threshold(chip);
+	} else {
+		max17042_set_soc_threshold(chip, 1);
+	}
+}
+
 static irqreturn_t max17042_thread_handler(int id, void *dev)
 {
 	struct max17042_chip *chip = dev;
@@ -871,9 +897,9 @@ static irqreturn_t max17042_thread_handler(int id, void *dev)
 	if (ret)
 		return IRQ_HANDLED;
 
-	if ((val & STATUS_SMN_BIT) || (val & STATUS_SMX_BIT)) {
+	if ((val & STATUS_SMN_BIT) || (val & STATUS_SMX_BIT) || (val & STATUS_DSOCI_BIT)) {
 		dev_dbg(&chip->client->dev, "SOC threshold INTR\n");
-		max17042_set_soc_threshold(chip, 1);
+		max17042_update_soc_threshold(chip);
 	}
 
 	/* we implicitly handle all alerts via power_supply_changed */
@@ -1118,7 +1144,12 @@ static int max17042_probe(struct i2c_client *client)
 			regmap_update_bits(chip->regmap, MAX17042_CONFIG,
 					CONFIG_ALRT_BIT_ENBL,
 					CONFIG_ALRT_BIT_ENBL);
-			max17042_set_soc_threshold(chip, 1);
+			if (chip->chip_type == MAXIM_DEVICE_TYPE_MAX17055) {
+				regmap_update_bits(chip->regmap, MAX17055_Config2,
+						CONFIG2_DSOCI_BIT_ENBL,
+						CONFIG2_DSOCI_BIT_ENBL);
+			}
+			max17042_update_soc_threshold(chip);
 		} else {
 			client->irq = 0;
 			if (ret != -EBUSY)
